//
//  ViewController.swift
//  MarvelAPP
//
//  Created by Juan Pa on 28/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {
    
    
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblAltura: UILabel!
    @IBOutlet weak var lblPeso: UILabel!
    @IBOutlet weak var nombrePoke: UILabel!
    @IBOutlet weak var alturaPoke: UILabel!
    @IBOutlet weak var pesoPoke: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func consultarButton(_ sender: Any) {
        
        let pkID = arc4random_uniform(250) + 1
        
        let URL = "https://pokeapi.co/api/v2/pokemon/\(pkID)"
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nombrePoke.text = pokemon?.name ?? ""
                self.alturaPoke.text = "\(pokemon?.height ?? 0)"
                self.pesoPoke.text = "\(pokemon?.weight ?? 0)"
            }
            
        }
        
    }
    
}

