//
//  InfoPokemonViewController.swift
//  MarvelAPP
//
//  Created by Juan Pa on 3/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit

class InfoPokemonViewController: UIViewController {

    var pokemon:Pokemon?
    
    @IBOutlet weak var pokemonImageVIew: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        print(pokemon?.name ?? "")
        self.title = pokemon?.name
        
        let pkService = pokemonService()
        
        pkService.getPokemonImage(id: (pokemon?.id)!) { (pkImage) in
            
            self.pokemonImageVIew.image = pkImage
        }
    }
    
    

}
