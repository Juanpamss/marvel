//
//  Pokemon.swift
//  MarvelAPP
//
//  Created by Juan Pa on 29/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable{

    var id:Int?
    var name:String?
    var height:Double?
    var weight:Double?

    required init?(map: Map){

    }

    func mapping(map: Map){
        
        id <- map["id"]
        name <- map ["name"]
        height <- map["height"]
        weight <- map["weight"]
        
    }

}
