//
//  PokedexViewController.swift
//  MarvelAPP
//
//  Created by Juan Pa on 29/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{

    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    @IBOutlet weak var pokedexTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = pokemonService()
        service.delegate = self
        service.downloadPokemons()
    }
    
    //MARK:- TableView
    
    func numberOfSections(in tableView: UITableView) -> Int { //Número de secciones
        return 2
    }
    
    func get20FistPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { //Número de filas en cada sección
        
        /*switch section{
        case 0:
            return 5
        default:
            return 10
        }*/
        
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //Devuelve la información de cada fila
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCustomCell") as! PokemonTableViewCell
        
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        
        //cell.textLabel?.text = pokemonArray[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        pokemonIndex = indexPath.row
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! InfoPokemonViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section{
        case 0:
            return "Section 1"
        default:
            return "Section 2"
        }
    }
    
    
    
}
